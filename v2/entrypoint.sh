#!/bin/sh

echo "Preparing data"
dd if=/dev/zero of=/usr/share/nginx/html/zero_1m bs=1024 count=1000
dd if=/dev/zero of=/usr/share/nginx/html/zero_2m bs=1024 count=2000
dd if=/dev/zero of=/usr/share/nginx/html/zero_5m bs=1024 count=5000
dd if=/dev/zero of=/usr/share/nginx/html/zero_10m bs=1024 count=10000

echo "Starting webserver"
chmod 744 /usr/share/nginx/html/*
nginx

echo "Starting tor"
tor -f /etc/tor/torrc
