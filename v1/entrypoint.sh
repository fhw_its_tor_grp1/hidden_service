#!/bin/sh

echo "Starting service"
python3 /service/app.py &
echo "Starting tor"
tor -f /etc/tor/torrc
